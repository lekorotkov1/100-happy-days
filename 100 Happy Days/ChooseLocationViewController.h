//
//  ChooseLocationViewController.h
//  100 Happy Days
//
//  Created by Alexey Korotkov on 22.12.15.
//  Copyright © 2015 Alexey Korotkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HappyDayInfo+CoreDataProperties.h"

@interface ChooseLocationViewController : UIViewController

@property (nonatomic, strong) HappyDayInfo *happyDayInfo;

@end
