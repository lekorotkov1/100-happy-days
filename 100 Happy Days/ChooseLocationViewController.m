//
//  ChooseLocationViewController.m
//  100 Happy Days
//
//  Created by Alexey Korotkov on 22.12.15.
//  Copyright © 2015 Alexey Korotkov. All rights reserved.
//

#import "ChooseLocationViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "CoreData+MagicalRecord.h"

@interface ChooseLocationViewController ()

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;

@end

@implementation ChooseLocationViewController

#pragma mark View Controller Lyfecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.mapView.settings.compassButton = YES;
    self.mapView.settings.myLocationButton = YES;
}

- (IBAction)selectLocationPressed:(id)sender {
    CGPoint point = self.mapView.center;
    CLLocationCoordinate2D coordinate = [self.mapView.projection coordinateForPoint:point];
    self.happyDayInfo.latitude = [NSNumber numberWithDouble:coordinate.latitude];
    self.happyDayInfo.longtitude = [NSNumber numberWithDouble:coordinate.longitude];
    
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
    }];
}

@end
