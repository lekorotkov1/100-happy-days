//
//  HappyDayDetailViewController.h
//  100 Happy Days
//
//  Created by Alexey Korotkov on 3.12.15.
//  Copyright © 2015 Alexey Korotkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HappyDayInfo+CoreDataProperties.h"

@interface HappyDayDetailViewController : UIViewController

@property (nonatomic, strong) UIImage *image;
@property (strong, nonatomic) UIImageView *imageView;
@property (nonatomic, strong) HappyDayInfo *info;
@property (nonatomic, strong) HappyDay *happyDay;

@end
