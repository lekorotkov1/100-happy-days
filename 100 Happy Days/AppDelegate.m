//
//  AppDelegate.m
//  100 Happy Days
//
//  Created by Alexey Korotkov on 26.11.15.
//  Copyright © 2015 Alexey Korotkov. All rights reserved.
//

#import "AppDelegate.h"
#import "MagicalRecord+Setup.h"
#import "MagicalRecord+iCloud.h"
#import <GoogleMaps/GoogleMaps.h>
#import "NSPersistentStoreCoordinator+MagicalRecord.h"
#import "NSManagedObjectContext+MagicalRecord.h"

@interface AppDelegate ()

@end

static NSString *kGoogleApiKey = @"AIzaSyCszVS2kW2_osNP8qiLGIge0evmS8oMvLk";

@implementation AppDelegate

//AIzaSyCszVS2kW2_osNP8qiLGIge0evmS8oMvLk

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [MagicalRecord setupCoreDataStackWithiCloudContainer:@"iCloud.com.app.happydays" localStoreNamed:@"HappyDaysModel"];
    [GMSServices provideAPIKey:kGoogleApiKey];
    [self registerForiCloudNotifications];
    
    /*UILocalNotification *locNot = [[UILocalNotification alloc] init];
    NSTimeInterval interval; // Your Alarms Remaining time in seconds
    locNot.fireDate = [NSDate dateWithTimeIntervalSinceNow:interval];
    [[UIApplication sharedApplication] scheduleLocalNotification: locNot];
    
    if ( [application respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound);
        
        UIUserNotificationSettings * settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
        [application registerForRemoteNotifications];
        
    }*/
    
    [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
    
    return YES;
}

#pragma mark - Notification Observers
- (void)registerForiCloudNotifications {
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter addObserver:self
                           selector:@selector(storesWillChange:)
                               name:NSPersistentStoreCoordinatorStoresWillChangeNotification
                             object:[NSPersistentStoreCoordinator MR_defaultStoreCoordinator]];
    
    [notificationCenter addObserver:self
                           selector:@selector(persistentStoreDidImportUbiquitousContentChanges:)
                               name:NSPersistentStoreDidImportUbiquitousContentChangesNotification
                             object:[NSPersistentStoreCoordinator MR_defaultStoreCoordinator]];
}

# pragma mark - iCloud Support

/// Use these options in your call to -addPersistentStore:
- (NSDictionary *)iCloudPersistentStoreOptions {
    return @{ NSPersistentStoreUbiquitousContentNameKey : @"iCloudStore" };
}

- (void) persistentStoreDidImportUbiquitousContentChanges:(NSNotification *)changeNotification {
    NSManagedObjectContext *context = [NSManagedObjectContext MR_defaultContext];
    
    [context performBlock:^{
        [context mergeChangesFromContextDidSaveNotification:changeNotification];
    }];
}

- (void)storesWillChange:(NSNotification *)notification {
    NSManagedObjectContext *context = [NSManagedObjectContext MR_defaultContext];
    
    [context performBlockAndWait:^{
        NSError *error;
        
        if ([context hasChanges]) {
            BOOL success = [context save:&error];
            
            if (!success && error) {
                // perform error handling
                NSLog(@"%@",[error localizedDescription]);
            }
        }
        
        [context reset];
    }];
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reminder"
                                                        message:notification.alertBody
                                                       delegate:self cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

#pragma clang diagnostic pop

@end
