//
//  PreferencesViewController.m
//  100 Happy Days
//
//  Created by Alexey Korotkov on 19.12.15.
//  Copyright © 2015 Alexey Korotkov. All rights reserved.
//

#import "PreferencesViewController.h"

@interface PreferencesViewController ()

@property (weak, nonatomic) IBOutlet UIDatePicker *picker;
@property (weak, nonatomic) IBOutlet UISwitch *enableSwitch;

@end

@implementation PreferencesViewController

#pragma mark View Controller Lyfecycle

- (void)viewDidLoad {
    self.enableSwitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:@"Notifications Enabled"];
}

#pragma mark Actions

- (IBAction)confirmTimePressed:(id)sender {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"Notifications Enabled"] == YES) {
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *components = [calendar components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:self.picker.date];
        [calendar setTimeZone: [NSTimeZone defaultTimeZone]];
        NSDate *dateToFire = [calendar dateFromComponents:components];
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        [localNotification setFireDate: dateToFire];
        [localNotification setTimeZone: [NSTimeZone defaultTimeZone]];
        [localNotification setRepeatInterval: NSCalendarUnitDay];
        [localNotification setAlertBody:@"Don't forget to choose happy moment of today!!!"];
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Notifications have been successfully set-up!"
                                              message:nil
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:nil];
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    } else {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"You should firstly enable notifications!!"
                                              message:nil
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:nil];
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (IBAction)switchValueChanged:(id)sender {
    if ([(UISwitch *)sender isOn] == NO) {
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
    }
    [[NSUserDefaults standardUserDefaults] setBool:[(UISwitch *)sender isOn] forKey:@"Notifications Enabled"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

@end
