//
//  HappyDaysViewController.m
//  100 Happy Days
//
//  Created by Alexey Korotkov on 1.12.15.
//  Copyright © 2015 Alexey Korotkov. All rights reserved.
//

#import "HappyDaysViewController.h"
#import "AWCollectionViewDialLayout.h"
#import "HappyDay+CoreDataProperties.h"
#import "NSManagedObject+MagicalFinders.h"
#import "NSManagedObject+MagicalRecord.h"
#import "NSManagedObjectContext+MagicalRecord.h"
#import "NSManagedObjectContext+MagicalSaves.h"
#import "PopAnimator.h"
#import "HappyDayDetailViewController.h"
#import "HappyDayPhoto+CoreDataProperties.h"
#import "HappyDayInfo+CoreDataProperties.h"
#import <Photos/Photos.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "HappyDayPhotoIcon+CoreDataProperties.h"
#import "MapsViewController.h"
#import "NSPersistentStoreCoordinator+MagicalRecord.h"
#import "CustomScrollIndicatorView.h"

static const NSInteger kNumberOfDays = 100;
static const CGFloat kLayoutRadius = 390.f;
static const CGFloat kLayoutAngularSpacing = 14.3999996f;
static const CGFloat kCellSizeWidth = 240.f;
static const CGFloat kCellSizeHeight = 100.f;
static const CGFloat kLayoutXOffset = 220.f;
static const NSInteger kImageTag = 100;
static const NSInteger kNameTag = 101;
static const NSInteger kBorderTag = 102;
static const CGFloat kImageWidth = 100.f;
static const CGFloat kImageHeight = 100.f;


@interface HappyDaysViewController () <UIViewControllerTransitioningDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIScrollViewDelegate>

@property (nonatomic, strong) NSMutableDictionary *thumbnailCache;
@property (nonatomic, strong) AWCollectionViewDialLayout *dialLayout;
@property (nonatomic) int type;
@property (nonatomic, strong) PopAnimator *animator;
@property (nonatomic) CGRect frameOfSelectedPhoto;
@property (nonatomic) NSIndexPath *selectedIndexPath;
@property (weak, nonatomic) IBOutlet CustomScrollIndicatorView *numberOfDays;
@property (weak, nonatomic) IBOutlet UILabel *currentLocationLabel;
@property NSArray *items;

@end

@implementation HappyDaysViewController

static NSString *cellId = @"cellId";

#pragma mark View Controller Lyfecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.type = 0;
    
    self.animator = [[PopAnimator alloc] init];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"dialCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:cellId];
    
    self.items = [HappyDay MR_findAll];
    
    self.dialLayout = [[AWCollectionViewDialLayout alloc] initWithRadius:kLayoutRadius andAngularSpacing:kLayoutAngularSpacing andCellSize:CGSizeMake(kCellSizeWidth, kCellSizeHeight) andAlignment:WHEELALIGNMENTCENTER andItemHeight:kCellSizeHeight andXOffset:kLayoutXOffset];
    [self.collectionView setCollectionViewLayout:self.dialLayout];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                           selector:@selector(storesDidChange:)
                               name:NSPersistentStoreCoordinatorStoresDidChangeNotification
                             object:[NSPersistentStoreCoordinator MR_defaultStoreCoordinator]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                           selector:@selector(persistentStoreDidImportUbiquitousContentChanges:)
                               name:NSPersistentStoreDidImportUbiquitousContentChangesNotification
                             object:[NSPersistentStoreCoordinator MR_defaultStoreCoordinator]];
}

#pragma mark iCloud + Core Data

- (void)storesDidChange:(NSNotification *)notification {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.items = [HappyDay MR_findAll];
        [self.collectionView reloadData];
    });
}

- (void) persistentStoreDidImportUbiquitousContentChanges:(NSNotification *)changeNotification {
    NSManagedObjectContext *context = [NSManagedObjectContext MR_defaultContext];
    
    [context performBlock:^{
        [context mergeChangesFromContextDidSaveNotification:changeNotification];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.items = [HappyDay MR_findAll];
            [self.collectionView reloadData];
        });
    }];
}

#pragma mark Collection View Datasource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return kNumberOfDays;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
    UIView *borderView = [cell viewWithTag:kBorderTag];
    borderView.layer.borderWidth = 1;
    
    if (indexPath.row < self.items.count) {
        borderView.layer.borderColor = [UIColor greenColor].CGColor;
        
        UIImageView *imgView = (UIImageView*)[cell viewWithTag:kImageTag];
        
        HappyDay *happyDay = self.items[indexPath.row];
        UIImage *image =[UIImage imageWithData:happyDay.photoIcon.image];
        
        if (image) {
            [imgView setImage:image];
        } else {
            [imgView setImage:[[UIImage alloc] init]];
        }
        
        UILabel *nameLabel = (UILabel*)[cell viewWithTag:kNameTag];
        
        if (happyDay.title) {
            [nameLabel setText:happyDay.title];
        } else {
            [nameLabel setText:[NSString stringWithFormat:@"Day #%ld",(long)indexPath.row + 1]];
        }
    } else {
        borderView.layer.borderColor = [UIColor redColor].CGColor;
        UIImageView *imgView = (UIImageView*)[cell viewWithTag:kImageTag];
        [imgView setImage:[[UIImage alloc] init]];
        
        NSString *playerName = [NSString stringWithFormat:@"Day #%ld",(long)indexPath.row + 1];
        UILabel *nameLabel = (UILabel*)[cell viewWithTag:kNameTag];
        [nameLabel setText:playerName];
    }
    
    return cell;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

#pragma mark UIViewController animation transitioning

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    self.animator.originFrame = self.frameOfSelectedPhoto;
    self.animator.presenting = YES;
    return self.animator;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    self.animator.presenting = NO;
    return self.animator;
}

#pragma mark Scroll View Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.collectionView) {
        NSArray *collectionViewCells = [self.collectionView visibleCells];
        UICollectionViewCell *nearestCellToTheCenter = [collectionViewCells firstObject];
        CGFloat minimumDistanceToCenter = ABS((nearestCellToTheCenter.frame.origin.y - self.collectionView.contentOffset.y + nearestCellToTheCenter.frame.size.height / 2 - self.collectionView.bounds.size.height / 2));
        for (UICollectionViewCell *cell in collectionViewCells) {
            NSLog(@"to center %f",ABS((cell.frame.origin.y - self.collectionView.contentOffset.y + cell.frame.size.height / 2 - self.collectionView.bounds.size.height / 2)));
            if (minimumDistanceToCenter > ABS((cell.frame.origin.y - self.collectionView.contentOffset.y  + cell.frame.size.height / 2 - self.collectionView.bounds.size.height / 2))) {
                nearestCellToTheCenter = cell;
                minimumDistanceToCenter = ABS((cell.frame.origin.y - self.collectionView.contentOffset.y  + cell.frame.size.height / 2 - self.view.bounds.size.height / 2));
            }
        }
        self.numberOfDays.counter = [self.collectionView indexPathForCell:nearestCellToTheCenter].row;
        [self.numberOfDays setNeedsDisplay];
        self.currentLocationLabel.text = [NSString stringWithFormat:@"%d",[self.collectionView indexPathForCell:nearestCellToTheCenter].row + 1];
    }
}

#pragma mark Collection View Delegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(kCellSizeWidth, kCellSizeHeight);
}


- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0 , 0, 0, 0);
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"didEndDisplayingCell:%i", (int)indexPath.item);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.items.count) {
        
        UIAlertController *controller = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *takePhotoAction = [UIAlertAction actionWithTitle:@"Take Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:indexPath];
            UIView *borderView = [cell viewWithTag:kBorderTag];
            self.frameOfSelectedPhoto = [borderView.superview convertRect:borderView.frame toView:nil];
            
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = NO;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            picker.transitioningDelegate = self;
            [self presentViewController:picker animated:YES completion:NULL];
        }];
        
        UIAlertAction *chooseFromGalleryAction = [UIAlertAction actionWithTitle:@"Choose from gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = NO;
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            picker.transitioningDelegate = self;
            [self presentViewController:picker animated:YES completion:NULL];
        }];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [controller addAction:takePhotoAction];
        [controller addAction:chooseFromGalleryAction];
        [controller addAction:cancelAction];
        
        [self presentViewController:controller animated:YES completion:nil];
    } else if (indexPath.row > self.items.count) {
        UIAlertController *controller = [[UIAlertController alloc] init];
        [controller setTitle:@"You can't add happy moments to future days"];
        [controller addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:controller animated:YES completion:nil];
    } else {
        UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:indexPath];
        UIView *borderView = [cell viewWithTag:kBorderTag];
        self.frameOfSelectedPhoto = [borderView.superview convertRect:borderView.frame toView:nil];
        
        HappyDayDetailViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HappyDayDetailViewController"];
        
        HappyDay *happyDay = self.items[indexPath.row];
        vc.image = [UIImage imageWithData:happyDay.photo.image];
        vc.transitioningDelegate = self;
        vc.info = happyDay.info;
        vc.happyDay = happyDay;
        
        [self presentViewController:vc animated:YES completion:nil];
    }
}

#pragma mark Image Picker Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    
    HappyDay *happyDay = [HappyDay MR_createEntity];
    happyDay.dayNumber = [NSNumber numberWithInt:self.items.count+1];
    
    HappyDayPhoto *photo = [HappyDayPhoto MR_createEntity];
    photo.image = UIImageJPEGRepresentation(image, 1);;
    happyDay.photo = photo;
    
    HappyDayInfo *happyDayInfo = [HappyDayInfo MR_createEntity];
    happyDay.info = happyDayInfo;
    
    HappyDayPhotoIcon *happyDayPhotoIcon = [HappyDayPhotoIcon MR_createEntity];
    happyDay.photoIcon = happyDayPhotoIcon;
    UIImage *image1 = [self scale:image toSize:CGSizeMake(kImageWidth, kImageHeight)];
    
    happyDayPhotoIcon.image = UIImageJPEGRepresentation(image1, 1);
    
    if (info[@"UIImagePickerControllerReferenceURL"]) {
        PHFetchResult *result = [PHAsset fetchAssetsWithALAssetURLs:@[info[@"UIImagePickerControllerReferenceURL"]] options:nil];
        PHAsset *asset = [result lastObject];
        
        happyDayInfo.latitude = [NSNumber numberWithDouble:asset.location.coordinate.latitude];
        happyDayInfo.longtitude = [NSNumber numberWithDouble:asset.location.coordinate.longitude];
        
        
    }
    
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        self.items = [HappyDay MR_findAll];
        [self.collectionView reloadData];
        [picker dismissViewControllerAnimated:YES completion:NULL];
        dispatch_async(dispatch_get_main_queue(), ^{
            
        });
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark UIImage Helper

- (UIImage *)scale:(UIImage *)image toSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.width)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}

#pragma mark Unwinding methods

- (IBAction)unwindToHappyDaysViewController:(UIStoryboardSegue *)unwindSegue
{
}

@end
