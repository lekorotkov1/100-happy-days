//
//  HappyDayPhotoIcon+CoreDataProperties.h
//  100 Happy Days
//
//  Created by Alexey Korotkov on 16.12.15.
//  Copyright © 2015 Alexey Korotkov. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "HappyDayPhotoIcon.h"

NS_ASSUME_NONNULL_BEGIN

@interface HappyDayPhotoIcon (CoreDataProperties)

@property (nullable, nonatomic, retain) NSData *image;
@property (nullable, nonatomic, retain) HappyDay *happyDay;

@end

NS_ASSUME_NONNULL_END
