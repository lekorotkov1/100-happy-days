//
//  MapsViewController.h
//  100 Happy Days
//
//  Created by Alexey Korotkov on 17.12.15.
//  Copyright © 2015 Alexey Korotkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface MapsViewController : UIViewController

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;

@end
