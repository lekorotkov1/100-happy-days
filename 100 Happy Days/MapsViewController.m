//
//  MapsViewController.m
//  100 Happy Days
//
//  Created by Alexey Korotkov on 17.12.15.
//  Copyright © 2015 Alexey Korotkov. All rights reserved.
//

#import "MapsViewController.h"
#import "HappyDayInfo+CoreDataProperties.h"
#import "NSManagedObject+MagicalFinders.h"

@interface MapsViewController () <UINavigationControllerDelegate, GMSMapViewDelegate>

@property (nonatomic, strong) NSMutableArray <HappyDayInfo *> *locationsArray;

@end

@implementation MapsViewController

#pragma mark View Controller Lyfecycle

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSArray *infoArray = [HappyDayInfo MR_findAll];
    
    self.locationsArray = [NSMutableArray new];
    
    self.mapView.delegate = self;
    
    for (HappyDayInfo *info in infoArray) {
        if ([info.latitude doubleValue] != 0 && [info.longtitude doubleValue] != 0) {
            [self.locationsArray addObject:info];
        }
    }
    
    if (self.locationsArray.count > 0) {
        CLLocationCoordinate2D firstLocation = CLLocationCoordinate2DMake([[[self.locationsArray firstObject] latitude] doubleValue], [[[self.locationsArray firstObject] longtitude] doubleValue]);
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = firstLocation;
        marker.infoWindowAnchor = CGPointMake(0.44f, 0.45f);
        marker.map = self.mapView;
        
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:firstLocation coordinate:firstLocation];
        
        [self.locationsArray removeObject:[self.locationsArray firstObject]];
        
        for (HappyDayInfo *info in self.locationsArray) {
            bounds = [bounds includingCoordinate:CLLocationCoordinate2DMake([[info latitude] doubleValue], [[info longtitude] doubleValue])];
            GMSMarker *marker = [[GMSMarker alloc] init];
            marker.position = CLLocationCoordinate2DMake([[info latitude] doubleValue], [[info longtitude] doubleValue]);
            marker.infoWindowAnchor = CGPointMake(0.44f, 0.45f);
            marker.map = self.mapView;
        }
        
        [self.mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:30.f]];
    } else {
        UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"You have no photos with location metadata" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [controller addAction:okAction];
        [self presentViewController:controller animated:YES completion:nil];
    }
}

@end
