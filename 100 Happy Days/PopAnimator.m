//
//  PopAnimator.m
//  100 Happy Days
//
//  Created by Alexey Korotkov on 3.12.15.
//  Copyright © 2015 Alexey Korotkov. All rights reserved.
//

#import "PopAnimator.h"

static const NSTimeInterval kTransitionDuration = 1.0;
static const CGFloat kSpringDamping = 0.4f;
static const CGFloat kExpectedCornerRadius = 50.f;

@implementation PopAnimator

- (instancetype)init {
    if (self = [super init]) {
        _originFrame = CGRectZero;
    }
    return self;
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return kTransitionDuration;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    UIView *containerView = [transitionContext containerView];
    UIView *toView = [transitionContext viewForKey:UITransitionContextToViewKey];
    UIView *happyMomentView = self.presenting ? toView : [transitionContext viewForKey:UITransitionContextFromViewKey];
    
    CGRect initialFrame = self.presenting ? self.originFrame : happyMomentView.frame;
    CGRect finalFrame = self.presenting ? happyMomentView.frame : self.originFrame;
    
    CGFloat yScaleFactor = self.presenting ? initialFrame.size.height / finalFrame.size.height : finalFrame.size.height / initialFrame.size.height;
    CGFloat xScaleFactor = self.presenting ? initialFrame.size.width / finalFrame.size.width : finalFrame.size.width / initialFrame.size.width;
    
    CGAffineTransform scaleTransform = CGAffineTransformMakeScale(xScaleFactor, yScaleFactor);
    
    if (self.presenting == YES) {
        happyMomentView.transform = scaleTransform;
        happyMomentView.center = CGPointMake(CGRectGetMidX(initialFrame), CGRectGetMidY(initialFrame));
        happyMomentView.clipsToBounds = YES;
    }
    
    [containerView addSubview:toView];
    [containerView bringSubviewToFront:happyMomentView];
    
    [UIView animateWithDuration:kTransitionDuration delay:0.0 usingSpringWithDamping:kSpringDamping initialSpringVelocity:0.0 options:UIViewAnimationOptionLayoutSubviews animations:^{
        happyMomentView.transform = self.presenting ? CGAffineTransformIdentity : scaleTransform;
        
        happyMomentView.center = CGPointMake(CGRectGetMidX(finalFrame), CGRectGetMidY(finalFrame));
    } completion:^(BOOL finished) {
        [transitionContext completeTransition:YES];
    }];
    
    CABasicAnimation *round = [CABasicAnimation animationWithKeyPath:@"cornerRadius"];
    round.fromValue = self.presenting ? [NSNumber numberWithDouble:kExpectedCornerRadius / xScaleFactor ]: [NSNumber numberWithDouble:0.0];
    round.toValue = self.presenting ? [NSNumber numberWithDouble:0.0] : [NSNumber numberWithDouble:kExpectedCornerRadius/xScaleFactor];
    
    [happyMomentView.layer addAnimation:round forKey:nil];
    happyMomentView.layer.cornerRadius = self.presenting ? 0.0 : kExpectedCornerRadius / xScaleFactor;
}

@end
