//
//  HappyDay+CoreDataProperties.h
//  100 Happy Days
//
//  Created by Alexey Korotkov on 16.12.15.
//  Copyright © 2015 Alexey Korotkov. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "HappyDay.h"

NS_ASSUME_NONNULL_BEGIN

@interface HappyDay (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *dayNumber;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) HappyDayInfo *info;
@property (nullable, nonatomic, retain) HappyDayPhoto *photo;
@property (nullable, nonatomic, retain) HappyDayPhotoIcon *photoIcon;

@end

NS_ASSUME_NONNULL_END
