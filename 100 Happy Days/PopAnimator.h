//
//  PopAnimator.h
//  100 Happy Days
//
//  Created by Alexey Korotkov on 3.12.15.
//  Copyright © 2015 Alexey Korotkov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PopAnimator : NSObject <UIViewControllerAnimatedTransitioning>

@property (nonatomic) BOOL presenting;
@property (nonatomic) CGRect originFrame;

@end
