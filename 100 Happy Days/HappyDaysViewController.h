//
//  HappyDaysViewController.h
//  100 Happy Days
//
//  Created by Alexey Korotkov on 1.12.15.
//  Copyright © 2015 Alexey Korotkov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HappyDaysViewController : UIViewController

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;

@end
