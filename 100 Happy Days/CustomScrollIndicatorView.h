//
//  CustomScrollIndicatorView.h
//  100 Happy Days
//
//  Created by Alexey Korotkov on 23.12.15.
//  Copyright © 2015 Alexey Korotkov. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface CustomScrollIndicatorView : UIView

@property (nonatomic, strong) IBInspectable UIColor *outlineColor;

@property (nonatomic, strong) IBInspectable UIColor *counterColor;
@property (nonatomic) IBInspectable NSInteger counter;

@end
