//
//  AppDelegate.h
//  100 Happy Days
//
//  Created by Alexey Korotkov on 26.11.15.
//  Copyright © 2015 Alexey Korotkov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

