//
//  HappyDayInfo+CoreDataProperties.m
//  100 Happy Days
//
//  Created by Alexey Korotkov on 16.12.15.
//  Copyright © 2015 Alexey Korotkov. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "HappyDayInfo+CoreDataProperties.h"

@implementation HappyDayInfo (CoreDataProperties)

@dynamic longtitude;
@dynamic latitude;
@dynamic newRelationship;

@end
