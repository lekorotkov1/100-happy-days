//
//  HappyDayDetailViewController.m
//  100 Happy Days
//
//  Created by Alexey Korotkov on 3.12.15.
//  Copyright © 2015 Alexey Korotkov. All rights reserved.
//

#import "HappyDayDetailViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "ChooseLocationViewController.h"
#import "HappyDay+CoreDataProperties.h"
#import "NSManagedObjectContext+MagicalRecord.h"
#import "NSManagedObjectContext+MagicalSaves.h"

@interface HappyDayDetailViewController () <GMSMapViewDelegate>

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (strong, nonatomic) IBOutlet UINavigationBar *navigation;

@end

@implementation HappyDayDetailViewController

#pragma mark View Controller Lyfecycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigation.topItem.title = self.happyDay.title;
    
    [self.imageView removeFromSuperview];
    
    self.imageView = [[UIImageView alloc] initWithFrame:[self displayedImageBounds]];
    [self.view addSubview:self.imageView];
    [self.view sendSubviewToBack:self.imageView];
    
    [self.imageView setImage:self.image];
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(closeAction)]];
    self.mapView.delegate = self;
    
    if ([self.info.latitude doubleValue] == 0 && [self.info.longtitude doubleValue] == 0) {
        self.mapView.hidden = YES;
    } else {
        self.mapView.hidden = NO;
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([self.info.latitude doubleValue], [self.info.longtitude doubleValue]);
    
        self.mapView.camera = [[GMSCameraPosition alloc]initWithTarget:coordinate zoom:15 bearing:0 viewingAngle:0];
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = coordinate;
        marker.infoWindowAnchor = CGPointMake(0.44f, 0.45f);
        marker.map = self.mapView;
    }
}

#pragma mark Actions

- (void)closeAction {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)renamePressed:(id)sender {
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Rename Happy Day"
                                          message:nil
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleCancel
                                   handler:nil];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   UITextField *login = alertController.textFields.firstObject;
                                   self.happyDay.title = login.text;
                                   [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                                       self.navigation.topItem.title = login.text;
                                   }];
                               }];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.text = self.happyDay.title;
     }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark Helper methods

- (CGRect)displayedImageBounds {
    UIImage *image = [self image];
    
    CGFloat boundsWidth  = [self.view bounds].size.width,
    boundsHeight = [self.view bounds].size.height;
    
    CGSize  imageSize  = [image size];
    CGFloat imageRatio = imageSize.width / imageSize.height;
    CGFloat viewRatio  = boundsWidth / boundsHeight;
    
    if(imageRatio < viewRatio) {
        CGFloat scale = boundsHeight / imageSize.height;
        CGFloat width = scale * imageSize.width;
        CGFloat topLeftX = (boundsWidth - width) * 0.5;
        return CGRectMake(topLeftX, 0, width, boundsHeight);
    }
    
    CGFloat scale = boundsWidth / imageSize.width;
    CGFloat height = scale * imageSize.height;
    CGFloat topLeftY = (boundsHeight - height) * 0.5;
    
    return CGRectMake(0, topLeftY, boundsWidth, height);
}

#pragma mark Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"addPositionSegue"]) {
        ChooseLocationViewController *vc = (ChooseLocationViewController *)segue.destinationViewController;
        vc.happyDayInfo = self.info;
    }
}

- (IBAction)unwindToHappyDayDetailViewController:(UIStoryboardSegue *)unwindSegue
{
}

@end
