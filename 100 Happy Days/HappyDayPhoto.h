//
//  HappyDayPhoto.h
//  100 Happy Days
//
//  Created by Alexey Korotkov on 16.12.15.
//  Copyright © 2015 Alexey Korotkov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class HappyDay;

NS_ASSUME_NONNULL_BEGIN

@interface HappyDayPhoto : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "HappyDayPhoto+CoreDataProperties.h"
